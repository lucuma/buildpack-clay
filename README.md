# Clay Buildpack
Based on [buildpack-nginx](https://github.com/rhy-jot/buildpack-nginx)

## Structure
* .clay - File: its presence signals that this buildpack should be used
* www - Folder: holds all files to be served by nginx
* nginx.conf.erb - Optional File: overrides `conf/nginx.conf.erb`
* mime.types - Optional File: overrides `conf/mime.types`
* custom-build - Optional File: executes commands before build is finished
